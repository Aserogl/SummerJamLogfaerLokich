using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    public class SetTargetFramerate : MonoBehaviour
    {
        [SerializeField] private InputField _input;

        private void Start()
        {
            int targetFrameRate = PlayerPrefs.GetInt("targetFrameRate", -1);

            if (targetFrameRate is 0 or -1)
            {
                SetInfinityFramerate();
                return;
            }
            Application.targetFrameRate = targetFrameRate;
            _input.text = targetFrameRate.ToString();
        }

        public void SetTargetFPS()
        {
            if (!int.TryParse(_input.text, out int result))
            {
                SetInfinityFramerate();
            }
            
            if (result is 0 or -1)
            {
                SetInfinityFramerate();
                return;
            }
            SetFramerate(result);

        }

        private void SetInfinityFramerate()
        {
            _input.text = "бесконечно";
            Application.targetFrameRate = -1;
            PlayerPrefs.SetInt("targetFrameRate", -1);
        }

        private void SetFramerate(int framerate)
        {
            Application.targetFrameRate = framerate;
            PlayerPrefs.SetInt("targetFrameRate", framerate);
        }
    }
}
