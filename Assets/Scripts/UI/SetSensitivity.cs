using System.Globalization;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    public class SetSensitivity : MonoBehaviour
    {
        [SerializeField] private InputField _sensitivityX;
        [SerializeField] private InputField _sensitivityY;

        private void Start()
        {
            _sensitivityX.text = PlayerPrefs.GetFloat("sensitivityX", 1f)
                .ToString(CultureInfo.InvariantCulture);
            _sensitivityY.text = PlayerPrefs.GetFloat("sensitivityY", 1f)
                .ToString(CultureInfo.InvariantCulture);
        }

        public void SetSensitivityX()
        {
            SetSens(_sensitivityX, "sensitivityX");
        }

        public void SetSensitivityY()
        {
            SetSens(_sensitivityY, "sensitivityY");
        }

        private void SetSens(InputField inputField, string prefsKey)
        {
            try
            {
                float result = float.Parse(inputField.text, CultureInfo.InvariantCulture);
                PlayerPrefs.SetFloat(prefsKey, result);
            }
            catch
            {
                inputField.text = "1";
            }
        }
    }
}
