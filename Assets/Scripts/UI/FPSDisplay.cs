using System.Collections;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    [RequireComponent(typeof(Text))]
    public class FPSDisplay : MonoBehaviour
    {
        [SerializeField] private float _updateTime = 1f;
        private Text _fpsText;
 
        private void Start()
        {
            _fpsText = GetComponent<Text>();
            StartCoroutine(UpdateFPS());
        }
 
        private IEnumerator UpdateFPS()
        {
            while (true)
            {
                int fps = (int)(1f / Time.deltaTime);
                DisplayFPS(fps);
 
                yield return new WaitForSeconds(_updateTime);
            }
        }
 
        private void DisplayFPS(int fps)
        {
            _fpsText.text = $"{fps} FPS";
        }
    }
}
