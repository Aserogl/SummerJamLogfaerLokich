using UnityEngine;

namespace UI
{
    [RequireComponent(typeof(RectTransform))]
    public class ScrollingStats : MonoBehaviour
    {
        [Min(0f)] [SerializeField] private float _speed;
        private float _startX;
        private RectTransform _rectTransform;

        private void Start()
        {
            _rectTransform = GetComponent<RectTransform>();
            _startX = _rectTransform.localPosition.x;
        }

        private void FixedUpdate()
        {
            _rectTransform.localPosition += Vector3.left * (Time.fixedDeltaTime * _speed);
            if (_rectTransform.localPosition.x < -_startX)
            {
                _rectTransform.localPosition = new Vector3(
                    _startX,
                    _rectTransform.localPosition.y,
                    _rectTransform.localPosition.z);
            }
        }
    }
}
