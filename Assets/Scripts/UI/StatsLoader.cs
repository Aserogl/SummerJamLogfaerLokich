using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    [RequireComponent(typeof(Text))]
    public class StatsLoader : MonoBehaviour
    {
        [SerializeField] private Managers.StatisticManager _statisticManager;
        
        private void Start()
        {
            UpdateText();
        }

        public void UpdateText()
        {
            GetComponent<Text>().text = _statisticManager.GetStatisticAsJson();
        }
    }
}
