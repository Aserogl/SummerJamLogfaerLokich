using UnityEngine;

namespace UI
{
    [RequireComponent(typeof(RectTransform))]
    public class ScrollingCredits : MonoBehaviour
    {
        [Min(0.0001f)] [SerializeField] private float _scrollSpeed;
        private RectTransform _rectTransform;
        private float _startY;

        private void Start()
        {
            _rectTransform = GetComponent<RectTransform>();
            _startY = _rectTransform.localPosition.y;
        }

        private void FixedUpdate()
        {
            _rectTransform.localPosition += new Vector3(
                0,
                Time.fixedDeltaTime * _scrollSpeed,
                0);
        }

        public void ResetPos()
        {
            _rectTransform.localPosition = new Vector3(
                _rectTransform.localPosition.x,
                _startY,
                _rectTransform.localPosition.x);
        }
    }
}
