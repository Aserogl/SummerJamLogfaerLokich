using UnityEngine;

[RequireComponent(typeof(ParticleSystem))]
public class DestroyOnParticleOver : MonoBehaviour
{
    private void Start()
    {
        Destroy(gameObject, GetComponent<ParticleSystem>().main.duration);
    }
}
