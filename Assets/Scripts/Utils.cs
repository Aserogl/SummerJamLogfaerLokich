﻿using UnityEngine;

public static class Utils
{
    public static T Choice<T>(T[] array)
    {
        return array[Random.Range(0, array.Length)];
    }

    public static Vector2 Vec3ToVec2Xz(Vector3 vec)
    {
        return new Vector2(vec.x, vec.z);
    }

    public static Vector3 Vec3XyzToVec3Xz(Vector3 vec, float y = 0f)
    {
        return new Vector3(vec.x, y, vec.z);
    }

    public static void TryShowCursor()
    {
#if UNITY_STANDALONE
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;
#endif
    }
    
    public static void TryHideCursor()
    {
#if UNITY_STANDALONE
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
#endif
    }
}
