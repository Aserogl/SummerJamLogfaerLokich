using UnityEngine;

namespace Player
{
    public class VerticalMove : MonoBehaviour
    {
        [SerializeField] private AnimationCurve _moveCurve;
        [SerializeField] private float _height;
        [SerializeField] private float _time;
        [SerializeField] private float _rotationSpeed;
        private float _t;
        private float _offset;
        private Transform _selfTransform;

        private void Start()
        {
            _selfTransform = transform;
            _offset = _selfTransform.position.y;
        }

        private void FixedUpdate()
        {
            transform.Rotate(0, Time.fixedDeltaTime * _rotationSpeed, 0);
            _t += Time.fixedDeltaTime / _time;
            _t %= 1.000001f;
            float y = _offset + _moveCurve.Evaluate(_t) * _height;
            Vector3 position = _selfTransform.position;
            _selfTransform.position = new Vector3(
                position.x,
                y,
                position.z);
        }
    }
}
