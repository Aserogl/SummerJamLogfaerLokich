using UnityEngine;
using UnityEngine.UI;

namespace Player
{
    [RequireComponent(typeof(Image))]
    public class HealthBar : MonoBehaviour
    {
        [SerializeField] private Text _text;
        [SerializeField] private Image _image;
        
        private void OnValidate()
        {
            _image = GetComponent<Image>();
        }
        
        public void SetValue(int health, int maxHealth)
        {
            _text.text = $"<b>{health}/{maxHealth}</b> хп";
            _image.fillAmount = (float)health / maxHealth;
        }
    }
}