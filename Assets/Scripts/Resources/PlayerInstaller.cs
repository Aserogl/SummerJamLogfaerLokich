using UnityEngine;
using Zenject;

public class PlayerInstaller : MonoInstaller
{
    [SerializeField] private Player.Player _playerInstance;

    public override void InstallBindings()
    {
        Container
            .Bind<Player.Player>()
            .FromInstance(_playerInstance)
            .AsSingle()
            .NonLazy();

        Container.QueueForInject(_playerInstance);
    }
}
