using UnityEngine;
using Zenject;

public class GameManagerInstaller : MonoInstaller
{
    [SerializeField] private Managers.GameManager _gameManagerInstance;

    public override void InstallBindings()
    {
        Container
            .Bind<Managers.GameManager>()
            .FromInstance(_gameManagerInstance)
            .AsSingle();

        Container.QueueForInject(_gameManagerInstance);
    }
}
