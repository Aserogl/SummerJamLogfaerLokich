using UnityEngine;

namespace Enemy
{
    public class PatrolPath : MonoBehaviour
    {
        [SerializeField] private Transform[] _points;
        public Transform[] Points => _points;

        private void OnDrawGizmosSelected()
        {
            try
            {
                Transform oldPoint = _points[0];
                foreach (Transform point in _points)
                {
                    Gizmos.DrawLine(oldPoint.position, point.position);
                    oldPoint = point;
                }
                Gizmos.DrawLine(oldPoint.position, _points[0].position);
            }
            catch
            {
                // ignored
            }
        }
    }
}
