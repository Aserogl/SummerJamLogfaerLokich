using UnityEngine;
using Zenject;

namespace Enemy
{
    public class SoldierSpawner : MonoBehaviour
    {
        [SerializeField] private Enemy _soldierPrefab;
        [SerializeField] private float _spawnCooldown;
        [Inject] private DiContainer _diContainer;
        private float _time;

        private void Update()
        {
            _time += Time.deltaTime;
            if (_time < _spawnCooldown) return;
            _time = 0f;
            _diContainer.InstantiatePrefab(_soldierPrefab);
        }
    }
}
