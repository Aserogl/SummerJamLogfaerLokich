using UnityEngine;

namespace ToggleOnPlatform
{
    public class ToggleOnMobile : MonoBehaviour
    {
        private void Start()
        {
#if UNITY_ANDROID || UNITY_IOS
            gameObject.SetActive(false);
#endif
        }
    }
}
