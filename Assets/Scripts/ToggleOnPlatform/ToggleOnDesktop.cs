using UnityEngine;

namespace ToggleOnPlatform
{
    public class ToggleOnDesktop : MonoBehaviour
    {
        private void Start()
        {
#if UNITY_STANDALONE
            gameObject.SetActive(false);
#endif
        }
    }
}
