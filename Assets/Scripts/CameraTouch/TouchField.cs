using UnityEngine;
using UnityEngine.EventSystems;

namespace CameraTouch
{
    public class TouchField : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
    {
        public Vector2 touchDist { get; private set; }
        private Vector2 _pointerOld;
        private bool _isPressed;
        private int _pointerId;

    #if UNITY_ANDROID || UNITY_ISO
    private void Update()
    {
        if (!_isPressed)
        {
            touchDist = Vector2.zero;
            return;
        }

        if (_pointerId >= 0 && _pointerId < Input.touches.Length)
        {
            touchDist = Input.touches[_pointerId].position - _pointerOld;
            _pointerOld = Input.touches[_pointerId].position;
        }
        else
        {
            touchDist = new Vector2(Input.mousePosition.x, Input.mousePosition.y) - _pointerOld;
            _pointerOld = Input.mousePosition;
        }
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        _isPressed = true;
        _pointerId = eventData.pointerId;
        _pointerOld = eventData.position;
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        _isPressed = false;
    }
    #else
        public void OnPointerDown(PointerEventData eventData)
        {

        }

        public void OnPointerUp(PointerEventData eventData)
        {

        }
    #endif
    }
}
