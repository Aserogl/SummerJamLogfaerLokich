using Cinemachine;
using UnityEngine;

namespace CameraTouch
{
    public class CineTouch : MonoBehaviour
    {
        [SerializeField] private CinemachineFreeLook _camera;
        [SerializeField] private TouchField _touchField;
        [SerializeField] private Vector2 _sensitivity;

#if UNITY_ANDROID || UNITY_IOS
        private void Start()
        {
            _sensitivity.x = PlayerPrefs.GetFloat("sensitivityX", 1f);
            _sensitivity.y = PlayerPrefs.GetFloat("sensitivityY", 1f);
        }

        private void Update()
        {
            _camera.m_XAxis.Value += _touchField.touchDist.x
                                     * 200
                                     * _sensitivity.x
                                     * Time.deltaTime;
            _camera.m_YAxis.Value += _touchField.touchDist.y
                                     * _sensitivity.y
                                     * Time.deltaTime;
        }
#else
        private void Start()
        {
            _camera.m_XAxis.m_InputAxisName = "Mouse X";
            _camera.m_YAxis.m_InputAxisName = "Mouse Y";
        }
#endif
    }
}
