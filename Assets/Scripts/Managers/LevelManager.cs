using UnityEngine;
using UnityEngine.SceneManagement;

namespace Managers
{
    [CreateAssetMenu(fileName = "LevelManager", menuName = "ScriptableObjects/LevelManager")]
    public class LevelManager : ScriptableObject
    {
        public static void SwitchToScene(int index)
        {
            Utils.TryShowCursor();
            Time.timeScale = 1f;
            SceneManager.LoadScene(index);
        }

        public static void ReloadScene()
        {
            SwitchToScene(SceneManager.GetActiveScene().buildIndex);
        }

        public static void Quit()
        {
            Application.Quit();
        }
    }
}
