# Kamikaze Drone
![](/Screenshots/cover.png)


## Навигация
- [1. Скриншоты](#1-скриншоты)
- [2. Как установить](#2-как-установить)
  - [1. itch.io](#2-itchio)
  - [2. Из исходного кода](#2-из-исходного-кода)
- [3. Лог изменений](#3-лог-изменений)
- [4. Лицензия](#4-лицензия)
- [5. Внести свой вклад](5-внести-свой-вклад)


## 1. Скриншоты
*P.S.: эти скриншоты были сделаны в первой версии игры*

![скриншот 1 не загрузился :(](/Screenshots/screenshot1.png)
![скриншот 2 не загрузился :(](/Screenshots/screenshot2.png)
![скриншот 3 не загрузился :(](/Screenshots/screenshot3.png)
![скриншот 4 не загрузился :(](/Screenshots/screenshot4.png)
![скриншот 5 не загрузился :(](/Screenshots/screenshot5.png)


## 2. Как установить
### 1. itch.io
[<img src="/Screenshots/kamikaze_drone_itch_io.png" alt="Ссылка на страницу">](https://aserogl.itch.io/kamikaze-drone)

Инструкции по установке есть на странице

### 2. Из исходного кода
#### 1. Сначала склонируй репозиторий
```console
$ git clone https://notabug.org/Aserogl/SummerJamLogfaerLokich.git
```

#### 2. Затем открой проект с помощью Unity 2022.2.20f1
`Open > Add project from disk > /путь/до/репозитория/`

#### 3. И в конце собери проект
`Build settings > Build`


## 3. Лог изменений
Журнал изменений [здесь](/CHANGELOG.md)

## 4. Лицензия
GNU General Public License v3.0

Текст лицензии [здесь](/LICENSE)

## 5. Внести свой вклад
Как внести свой вклад в развитие игры написано [здесь](/CONTRIBUTING.md)
